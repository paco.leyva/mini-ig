import React, { useEffect } from 'react'
import Photo from '../Photo/Photo'
import { useGetPhotos } from '../../APIUtility'

import './css/styles.css'

const Feed = () => {
    const {
        isLoading,
        data,
        execute,
    } = useGetPhotos();

    useEffect(() => {
        try {
            execute({
                query: 'photo',
                perPage: 50
            })
        } catch (error) {
            console.log(console.error)
        }
        
    }, [execute])

    if (isLoading === null) {
        return <div><p><i class="fa fa-spinner w3-spin" style={{ fontSize: '64px' }}></i></p></div>;
    } 
    
    return (
        <div className="feed">
            <h2>BROWSE ALL</h2>
            <div className="masonry-container">
                {data && data.response.results.map(photo => (
                    <div key={photo.id} className="masonry-panel">
                        <Photo photo={photo} />
                    </div>
                ))}
            </div>
        </div>
    )

}

export default Feed